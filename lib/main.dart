import 'package:app2/src/routes/routes.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'App2',
      //home:
      initialRoute: 'home',
      routes: getApplicationRoutes(),
    );
  }
}
