import 'package:app2/src/routes/routes.dart';
import 'package:flutter/material.dart';

class SoportePage extends StatefulWidget {
  SoportePage({Key key}) : super(key: key);

  @override
  _SoportePageState createState() => _SoportePageState();
}

class _SoportePageState extends State<SoportePage> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.green,
          title: Text('Soporte'),
          actions: btnUser(context),
        ),
        body: Center(
          child: Container(
            child: Text('Soporte'),
          ),
        ),
      ),
    );
  }
}
