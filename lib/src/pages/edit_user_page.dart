import 'package:flutter/material.dart';

class EditUserPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.green,
          title: Text('HOLA USER'),
        ),
        body: Stack(
          children: <Widget>[
            _loginForm(context),
          ],
        ));
  }

  Widget _loginForm(BuildContext context) {
    return SingleChildScrollView(
        child: Column(
      children: <Widget>[
        SafeArea(
            child: Container(
          height: 30.0,
        )),
        _image(),
        Container(
          margin: EdgeInsets.symmetric(vertical: 20.0),
          padding: EdgeInsets.symmetric(vertical: 30.0),
          decoration: BoxDecoration(
              color: Colors.white30,
              borderRadius: BorderRadius.circular(5.0),
              boxShadow: <BoxShadow>[
                BoxShadow(
                  color: Colors.white,
                  blurRadius: 3.0,
                  offset: Offset(0.0, 5.0),
                  spreadRadius: 3.0,
                )
              ]),
          child: Column(
            children: <Widget>[
              _nombre(),
              SizedBox(
                height: 20.0,
              ),
              _apellidos(),
              SizedBox(
                height: 20.0,
              ),
              _numeroIdentidad(),
              SizedBox(
                height: 20.0,
              ),
              SizedBox(
                height: 20.0,
              ),
              _address(),
              SizedBox(
                height: 20.0,
              ),
              _email(),
              SizedBox(
                height: 20.0,
              ),
              _boton(context),
            ],
          ),
        )
      ],
    ));
  }

  Widget _image() {
    return Card(
      child: Column(
        children: <Widget>[Image(image: AssetImage('assets/user1.png'))],
      ),
    );
  }

  Widget _nombre() {
    return StreamBuilder(
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: TextField(
            decoration: InputDecoration(
                hintText: 'Ingresar Nombre',
                labelText: 'Nombre',
                counterText: snapshot.data),
          ),
        );
      },
    );
  }

  Widget _apellidos() {
    return StreamBuilder(
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: TextField(
            decoration: InputDecoration(
                hintText: 'Ingresar Apellidos',
                labelText: 'Apellidos',
                counterText: snapshot.data),
          ),
        );
      },
    );
  }

  Widget _numeroIdentidad() {
    return StreamBuilder(
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: TextField(
            keyboardType: TextInputType.number,
            decoration: InputDecoration(
                hintText: 'Ingresar numero de Identificación',
                labelText: 'Nro Identificación',
                counterText: snapshot.data),
          ),
        );
      },
    );
  }

  Widget _address() {
    return StreamBuilder(
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: TextField(
            obscureText: true,
            decoration: InputDecoration(
                hintText: 'Ingresar Dirección',
                labelText: 'Dirección',
                counterText: snapshot.data,
                errorText: snapshot.error),
          ),
        );
      },
    );
  }

  Widget _email() {
    return StreamBuilder(
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: TextField(
            obscureText: true,
            decoration: InputDecoration(
                hintText: 'Ingresar correo electrónico',
                labelText: 'Email',
                counterText: snapshot.data,
                errorText: snapshot.error),
          ),
        );
      },
    );
  }

  Widget _boton(BuildContext context) {
    return RaisedButton(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 80.0, vertical: 15.0),
        child: Text('Guardar'),
      ),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6.0)),
      elevation: 0.0,
      color: Colors.blue,
      textColor: Colors.white,
      onPressed: () => _accionSave(),
    );
  }

  _accionSave() {}
}
