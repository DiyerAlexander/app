import 'package:flutter/material.dart';
import 'package:app2/src/routes/routes.dart';

class NotificacionPage extends StatefulWidget {
  NotificacionPage({Key key}) : super(key: key);

  @override
  _NotificacionPageState createState() => _NotificacionPageState();
}

class _NotificacionPageState extends State<NotificacionPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.green,
          title: Text('NOTIFICACIONES'),
          actions: btnUser(context),
        ),
        body: ListView(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 50),
          children: <Widget>[_cardZone2()],
        ));
  }

  Widget _cardZone() {
    return Card(
      child: Column(
        children: <Widget>[
          ListTile(
            title: Text('Zona 1'),
            subtitle: Text('Radio Alcance  \nFecha de creacion'),
          ),
        ],
      ),
    );
  }

  Widget _cardZone2() {
    return Card(
        child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
            padding: EdgeInsets.all(30),
            child: Text(
                'JUAN PEREZ inicio y finalizo las medias de bioseguridad con exito')),
        Container(
            padding: EdgeInsets.all(30), child: Text('DD/MM/YY HH:MM:SS')),
      ],
    ));
  }
}
