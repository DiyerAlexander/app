import 'package:app2/src/pages/soporte_page.dart';
import 'package:app2/src/pages/zona_page.dart';
import 'package:flutter/material.dart';
import 'entrega_page.dart';
import 'notificaciones_page.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _callPage(currentIndex),
      bottomNavigationBar: _crearBottonNavigationbar(),
    );
  }

  Widget _callPage(int paginaActual) {
    switch (paginaActual) {
      case 0:
        return EntregaPage();
      case 1:
        return ZonaPage();
      case 2:
        return NotificacionPage();
      case 3:
        return SoportePage();

      default:
        return EntregaPage();
    }
  }

  Widget _crearBottonNavigationbar() {
    return BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      currentIndex: currentIndex,
      items: [
        BottomNavigationBarItem(
            icon: Icon(Icons.dehaze), title: Text('Mis Entregas')),
        BottomNavigationBarItem(
            icon: Icon(Icons.assistant_photo), title: Text('Zonas')),
        BottomNavigationBarItem(
            icon: Icon(Icons.notifications), title: Text('Notificaciones')),
        BottomNavigationBarItem(
            icon: Icon(Icons.device_unknown), title: Text('Soporte'))
      ],
      onTap: (index) {
        setState(() {
          currentIndex = index;
        });
      },
    );
  }
}
