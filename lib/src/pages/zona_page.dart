import 'package:app2/src/routes/routes.dart';
import 'package:flutter/material.dart';

class ZonaPage extends StatefulWidget {
  ZonaPage({Key key}) : super(key: key);

  @override
  _ZonaPageState createState() => _ZonaPageState();
}

class _ZonaPageState extends State<ZonaPage> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.green,
            title: Text('MIS ZONAS'),
            actions: btnUser(context),
          ),
          body: ListView(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 50),
            children: <Widget>[_inputNombre(), _cardZone()],
          )),
    );
  }

  Widget _inputNombre() {
    return Container(
      padding: EdgeInsets.all(50),
      child: Center(
        child: Text(
            'Define una zonas de alerta,te notificará cuando el vehículo de tu ruta de interes entre a dicha area.'),
      ),
    );
  }

  Widget _cardZone() {
    return Card(
      child: Column(
        children: <Widget>[
          ListTile(
            title: Text('Zona 1'),
            subtitle: Text('Radio Alcance  \nFecha de creacion'),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              FlatButton(
                textColor: Colors.white,
                color: Colors.blue,
                onPressed: () {},
                child: Text('Editar'),
              )
            ],
          )
        ],
      ),
    );
  }
}
