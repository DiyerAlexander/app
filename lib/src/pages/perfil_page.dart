import 'package:flutter/material.dart';

class PerfilPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green,
        title: Text('HOLA USER'),
      ),
      body: _lista(context),
    );
  }

  Widget _lista(BuildContext context) {
    return ListView(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 50),
      children: _listaItems(context),
    );
  }

  List<Widget> _listaItems(BuildContext context) {
    return [
      ListTile(
        title: Text('Datos de Perfil'),
        leading: Icon(Icons.account_circle),
        trailing: Icon(
          Icons.keyboard_arrow_right,
          color: Colors.green,
        ),
        onTap: () {
          Navigator.pushNamed(context, '/userData');
        },
      ),
      Divider(),
      ListTile(
        title: Text('Historial de Notificaciones'),
        leading: Icon(Icons.notifications),
        trailing: Icon(
          Icons.keyboard_arrow_right,
          color: Colors.green,
        ),
        onTap: () {
          Navigator.pushNamed(context, '/notifications');
        },
      ),
      Divider(),
      ListTile(
        title: Text('Registro Ingreso/Salida'),
        leading: Icon(Icons.wallpaper),
        trailing: Icon(
          Icons.keyboard_arrow_right,
          color: Colors.green,
        ),
        onTap: () {
          Navigator.pushNamed(context, '/register');
        },
      ),
      Divider(),
      ListTile(
        title: Text('Centro de Ayuda'),
        leading: Icon(Icons.device_unknown),
        trailing: Icon(
          Icons.keyboard_arrow_right,
          color: Colors.green,
        ),
        onTap: () {
          Navigator.pushNamed(context, '/centerSupport');
        },
      ),
      Divider(),
      ListTile(
        title: Text('Terminos y Condiciones'),
        leading: Icon(Icons.book),
        trailing: Icon(
          Icons.keyboard_arrow_right,
          color: Colors.green,
        ),
        onTap: () {
          Navigator.pushNamed(context, '/terms');
        },
      ),
      Divider(),
      ListTile(
        title: Text('Cerrar Sesión'),
        leading: Icon(Icons.exit_to_app),
        trailing: Icon(
          Icons.keyboard_arrow_right,
          color: Colors.green,
        ),
        onTap: () {
          Navigator.pushNamed(context, '/exit');
        },
      ),
    ];
  }
}
