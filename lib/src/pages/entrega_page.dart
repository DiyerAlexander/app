import 'package:flutter/material.dart';
import 'package:app2/src/routes/routes.dart';

class EntregaPage extends StatefulWidget {
  EntregaPage({Key key}) : super(key: key);

  @override
  _EntregaPageState createState() => _EntregaPageState();
}

class _EntregaPageState extends State<EntregaPage> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.green,
          title: Text('Entregas'),
          actions: btnUser(context),
        ),
        body: Center(
          child: Container(
            child: Text('Entregas'),
          ),
        ),
      ),
    );
  }
}
