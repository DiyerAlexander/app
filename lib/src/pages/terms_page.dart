import 'package:flutter/material.dart';

class TermsPage extends StatelessWidget {
  String textoCuerpo =
      "Toda la información se recoge con fines estrictamente de interés público ante la situación decretada por las Autoridades Públicas, para proteger y salvaguardar un interés esencial para la vida de las personas, en consecuencia, autorizo el manejo de la información aportada en este formulario de síntomas COVID-19 con el propósito de desarrollar acciones de promoción y prevención frente al riesgo de contagio acorde con lo normado por el Ministerio de Salud y las demás autoridades competentes. De conformidad con lo establecido en la Ley 1581 de 2012 de protección de datos personales, se podrá suministrar información a las entidades públicas o administrativas que en el ejercicio de sus funciones legales así lo requieran, o a las personas establecidas en el artículo 13 de la ley. Los datos proporcionados por el usuario deben ser veraces, completos, exactos, actualizados, comprobables y comprensibles y en consecuencia el usuario asume toda la responsabilidad sobre la falta de veracidad o exactitud de éstos. Este formulario es una guía de identificación de síntomas y signos de alarma que puedan estar relacionados con el coronavirus COVI D-19, pero en ningún caso reemplaza la atención médica ni las pruebas diagnósticas realizadas por el personal médico autorizado.";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.green,
          title: Text('TERMINOS Y CONDICIONES'),
        ),
        body: ListView(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 50),
          children: <Widget>[_cardZone()],
        ));
  }

  Widget _cardZone() {
    return Card(
      child: Column(
        children: <Widget>[
          ListTile(
            title: Text('AUTORIZACIÓN TRATAMIENTO DE DATOS \n \n'),
            subtitle: Text(textoCuerpo),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              FlatButton(
                onPressed: () {},
                child: Text('GUARDAR'),
              ),
              FlatButton(
                onPressed: () {},
                child: Text('CANCELAR'),
              )
            ],
          )
        ],
      ),
    );
  }
}
