import 'package:app2/src/pages/edit_user_page.dart';
import 'package:app2/src/pages/notificaciones_page.dart';
import 'package:app2/src/pages/perfil_page.dart';
import 'package:app2/src/pages/terms_page.dart';
import 'package:flutter/material.dart';
import 'package:app2/src/pages/home_page.dart';

Map<String, WidgetBuilder> getApplicationRoutes() {
  return <String, WidgetBuilder>{
    "home": (BuildContext context) => HomePage(),
    "/user": (BuildContext context) => PerfilPage(),
    "/terms": (BuildContext context) => TermsPage(),
    "/userData": (BuildContext context) => EditUserPage(),
    "/notifications": (BuildContext context) => NotificacionPage(),
  };
}

List<Widget> btnUser(context) {
  return <Widget>[
    IconButton(
        icon: Icon(Icons.account_circle),
        onPressed: () {
          Navigator.pushNamed(context, '/user');
        })
  ];
}
